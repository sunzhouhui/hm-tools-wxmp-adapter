package top.hmtools.wxmp.user.model;

public class TagFunsParam {

	private long tagid;
	
	private String next_openid;

	public long getTagid() {
		return tagid;
	}

	public void setTagid(long tagid) {
		this.tagid = tagid;
	}

	public String getNext_openid() {
		return next_openid;
	}

	public void setNext_openid(String next_openid) {
		this.next_openid = next_openid;
	}

	@Override
	public String toString() {
		return "TagFunsParam [tagid=" + tagid + ", next_openid=" + next_openid + "]";
	}
	
	
}
