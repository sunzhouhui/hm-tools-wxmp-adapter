package top.hmtools.wxmp.menu.models.eventMessage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseEventMessage;
import top.hmtools.wxmp.core.model.message.enums.Event;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * location_select：弹出地理位置选择器的事件推送
 * <br>
 * <p>
 * <xml><ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName>
<FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName>
<CreateTime>1408091189</CreateTime>
<MsgType><![CDATA[event]]></MsgType>
<Event><![CDATA[location_select]]></Event>
<EventKey><![CDATA[6]]></EventKey>
<SendLocationInfo><Location_X><![CDATA[23]]></Location_X>
<Location_Y><![CDATA[113]]></Location_Y>
<Scale><![CDATA[15]]></Scale>
<Label><![CDATA[ 广州市海珠区客村艺苑路 106号]]></Label>
<Poiname><![CDATA[]]></Poiname>
</SendLocationInfo>
</xml>
 * </p>
 * @author Hybomyth
 *
 */
@WxmpMessage(msgType=MsgType.event,event=Event.location_select)
public class LocationSelectEventMessage extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3507130637516011593L;
	
	/**
	 * 发送的位置信息
	 */
	@XStreamAlias("SendLocationInfo")
	private SendLocationInfo sendLocationInfo;

	/**
	 * 发送的位置信息
	 * @return
	 */
	public SendLocationInfo getSendLocationInfo() {
		return sendLocationInfo;
	}

	/**
	 * 发送的位置信息
	 * @param sendLocationInfo
	 */
	public void setSendLocationInfo(SendLocationInfo sendLocationInfo) {
		this.sendLocationInfo = sendLocationInfo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void configXStream(XStream xStream) {
		
	}

	

}
