package top.hmtools.wxmp.message.customerService.model.sendMessage;

/**
 * Auto-generated: 2019-08-25 18:15:23
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Wxcard {

    private String card_id;
    public void setCard_id(String card_id) {
         this.card_id = card_id;
     }
     public String getCard_id() {
         return card_id;
     }

}