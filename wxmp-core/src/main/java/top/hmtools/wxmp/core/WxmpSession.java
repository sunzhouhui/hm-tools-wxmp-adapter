package top.hmtools.wxmp.core;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import top.hmtools.wxmp.core.configuration.WxmpConfiguration;

public abstract class WxmpSession implements InvocationHandler{

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 全局配置
	 */
	protected WxmpConfiguration wxmpConfiguration;
	
	public WxmpSession(WxmpConfiguration config) {
		if(config == null){
			throw new RuntimeException("全局配置为空");
		}
		this.wxmpConfiguration = config;
	}
	
	/**
	 * 获取被动态代理的对象实例
	 * @param type
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> T getMapper(Class<T> type) {
		// 动态代理
		return (T) Proxy.newProxyInstance(type.getClassLoader(), new Class[] { type }, this);
	}
}
