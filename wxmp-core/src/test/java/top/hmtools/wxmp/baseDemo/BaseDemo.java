package top.hmtools.wxmp.baseDemo;

import org.junit.Test;

import top.hmtools.wxmp.BaseTest;
import top.hmtools.wxmp.baseDemo.model.MenuWapperDemoBean;

public class BaseDemo extends BaseTest {

	/**
	 * 测试获取微信公众号自定义菜单信息并打印
	 */
	@Test
	public void testGetMenu(){
		//获取请求微信公众号自定义菜单相关接口的mapper
		MenuMapperDemo menuMapperDemo = this.wxmpSession.getMapper(MenuMapperDemo.class);
		
		//请求微信公众号服务接口，获取自定义菜单信息，并格式化打印
		MenuWapperDemoBean menu = menuMapperDemo.getMenu();
		this.printFormatedJson("获取到的自定义菜单", menu);
	}
}
