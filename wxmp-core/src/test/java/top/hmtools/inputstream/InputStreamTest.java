package top.hmtools.inputstream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class InputStreamTest {

	@Test
	public void testHashcode() throws IOException{
		InputStream inputStream = IOUtils.toInputStream("hello,hybo","utf-8");
		//获取inputstream流中字节长度
		int available = inputStream.available();
		System.out.println(available);
		System.out.println(inputStream.hashCode());
		String string = IOUtils.toString(inputStream,"utf-8");
		System.out.println(string);
		System.out.println(inputStream.hashCode());
		string = IOUtils.toString(inputStream,"utf-8");
		System.out.println(string);
	}
	
	/**
	 * 测试获取inputstream字节长度
	 * @throws IOException
	 */
	@Test
	public void testGetInputStreamLength() throws IOException{
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("log4j2.xml");
		System.out.println("首次读取数据流：");
		System.out.println(inputStream.available());
		System.out.println(IOUtils.toString(inputStream,"utf-8"));

		System.out.println("再次读取数据流：");
		System.out.println(inputStream.available());
		System.out.println(IOUtils.toString(inputStream,"utf-8"));
	}
	
	/**
	 * 输入流反复读取测试
	 * @throws IOException
	 */
	@Test
	public void testInputStreamAAA() throws IOException{
		String content = "BoyceZhang!";  
		InputStream inputStream = new ByteArrayInputStream(content.getBytes());  

		// 判断该输入流是否支持mark操作  
		if (!inputStream.markSupported()) {  
		    System.out.println("mark/reset not supported!");  
		}  
		int ch;    
		boolean marked = false;    
		while ((ch = inputStream.read()) != -1) {  

		    //读取一个字符输出一个字符    
		    System.out.print((char)ch);    
		    //读到 'e'的时候标记一下  
		     if (((char)ch == 'e')& !marked) {    
		        inputStream.mark(content.length());  //先不要理会mark的参数  
		         marked = true;    
		     }    

		     //读到'!'的时候重新回到标记位置开始读  
		      if ((char)ch == '!' && marked) {    
		          inputStream.reset();    
		          marked = false;  
		      }    
		}  

		//程序最终输出：BoyceZhang!Zhang!  
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
