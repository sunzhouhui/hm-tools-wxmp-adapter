---
home: true
actionText: 快速上手 →
actionLink: /guide/
features:
- title: 优雅
  details: 基于spring boot starter，可单独部署，也可整合到您的项目工程，在maven pom.xml文件中引用即可用，可以零配置。
- title: 灵活
  details: 可以根据需要，在请求的URL中指定所需要的JavaScript或者css文件名称组合，还可以指定字符编码。
- title: 安全
  details: 支持使用yuicompressor对js，css文件内容进行实时压缩、混淆。
---

<icp/>


